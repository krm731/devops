*https://www.nomadproject.io/docs/vault-integration/index.html*
```# Download the policy and token role
$ curl https://nomadproject.io/data/vault/nomad-server-policy.hcl -O -s -L
$ curl https://nomadproject.io/data/vault/nomad-cluster-role.json -O -s -L

# Write the policy to Vault
$ vault policy write nomad-server nomad-server-policy.hcl

# Create the token role with Vault
$ vault write /auth/token/roles/nomad-cluster @nomad-cluster-role.json```

## consul config
```
{
  "key": {
    "vault/": {
      "policy": "write"
    }
  },
  "node": {
    "": {
      "policy": "write"
    }
  },
  "service": {
    "vault": {
      "policy": "write"
    }
  },
  "agent": {
    "": {
      "policy": "write"
    }

  },
  "session": {
    "": {
      "policy": "write"
    }
  }
}```