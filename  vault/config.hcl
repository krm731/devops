// https://www.vaultproject.io/docs/configuration/storage/consul.html
storage "consul" {
  address = "127.0.0.1:8500" //unix socket
  // address = "unix:///tmp/.consul.http.sock"
  path    = "vault"
}

//TODO telemetry 

