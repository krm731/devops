bind_addr = "0.0.0.0"
data_dir  = "/var/lib/nomad"
server {
  enabled          = true
  bootstrap_expect = 1
encrypt = "cg8StVXbQJ0gPvMd9o7yrg=="
authoritative_region = "global"
}
client {
  enabled       = true
  network_speed = 100
  servers = ["nomad.service.consul:4647"]
}
acl {
  enabled = true
  token_ttl = "30s"
  policy_ttl = "60s"
}
autopilot {
    cleanup_dead_servers = true
    last_contact_threshold = "200ms"
    max_trailing_logs = 250
    server_stabilization_time = "10s"
    enable_redundancy_zones = false
    disable_upgrade_migration = false
    enable_custom_upgrades = false
}

consul {
  address = "127.0.0.1:8500"
  auth    = "admin:password"
  token   = "abcd1234"
}

vault {
  enabled = true
  address = "https://vault.service.consul:8200"
}